import numpy as np


class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)
        
    def __repr__(self):
        return 'x: '+str(self.x)+', y: '+str(self.y)

    


class Noeud:
    def __init__(self, profondeur_max=np.infty):
        self.question = None
        self.enfants = {}
        self.profondeur_max = profondeur_max
        self.proba = None
        self.hauteur = None

    def prediction(self, x):
        return self.proba
        
    def grow(self, listeDP):

        entropie_data = Entropie(listeDP)    
        meilleurs_question = best_split(listeDP)   
        
        if entropie_data > 0 :
            self.question = meilleurs_question
            self.enfants[0] = Noeud(self.profondeur_max)
            self.enfants[1] = Noeud(self.profondeur_max)
            self.proba =proba_empirique(listeDP)
        else:
            self.question = meilleurs_question
            self.proba =proba_empirique(listeDP)


    def elagage(self, coefficient):

        if self.enfants != {}:

            self.enfants[0].elagage(coefficient)
            self.enfants[1].elagage(coefficient)

        else :
            if gain_entropie(???, self.enfats[0].question) < coefficient:
                del self.enfants[0]

            if gain_entropie(???, self.enfats[1].question) < coefficient:
                del self.enfants[1]

class ForetAleatoires:
    __init__(self):
        self.liste_noeud = []

    def prediction(self, x):
        pass

def load_data(filelocation):
    with open(filelocation,'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x,y,attributs))
    return data

def proba_empirique(listeDP):
    liste_proprotion = {}

    for elem in listeDP:
        classe = elem.y
        if classe in liste_proprotion:
            liste_proprotion[classe] += 1 / len(listeDP)
        else:
            liste_proprotion[classe] = 1 / len(listeDP)

    return liste_proprotion

def question_inf(x, attribut, s):

    if x[attribut] < s:
        return True
    else:
        return False

def split(listeDP, attribut, s):
    liste_true = []
    liste_false = []

    for elem in listeDP :
        if question_inf(elem.x, attribut, s) :
            liste_true.append(attribut, s)
        else :
            liste_false.append(attribut, s)

    return (liste_true, liste_false)


def list_separ_attributs(listeDP, attribut):
    pass


def list_questions(listeDP):

    liste_pertinente = []

    for elem in listeDP[0].x:
        liste_pertinente += list_separ_attributs(listeDP, elem)

    return liste_pertinente

def Entropie(listeDP):

    proportions = proba_empirique(listeDP)

    entropie = 0

    for i in range(len(proportions)) :
        entropie = entropie - (proportions[i] * np.log(proportions[i]) )

    return entropie

def gain_entropie(listeDP, question):
    pass

def best_split(listeDP):
    liste_quest = list_questions(listeDP)

    meilleurs_gain = 0

    for elem in liste_quest :
        if gain_entropie(listeDP, elem) > meilleurs_gain :
            meilleurs_gain = gain_entropie(listeDP, elem)
            meilleurs_question = elem

    return meilleurs_question

def prediction(listeDP, Noeud):
    pass
        
    

