import interfacelib
import numpy as np
import time

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interfacelib.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):
	
	def demande_coup(self):
		liste_des_coups = self.jeu.plateau.liste_coups_valides(self.couleurval)
		r = np.random.randint(0, len(liste_des_coups))
		return liste_des_coups[r]


class Minmax(IA):

	def demande_coup(self):

		time_debut = time.time()
		simu = self.min_max(self.jeu.plateau, True, 3)
		time_fin = time.time()
		delta = time_fin - time_debut

		
		nb_jouer = 0
		nb_jouer += simu [2]
		print("Pour ce coup,le joueur "+ self.couleur+ " a utiliser jouer "+ str(nb_jouer) +" fois et a mis " + str(delta) + " secondes")		
		# Ces deux compteurs sont liés si l'un augmente l'autre aussi 
		coup_choisie = simu [1]
		return coup_choisie
		
	def min_max(self, plateau, maximisateur, profondeur):
		#print("la profondeur est" +str(profondeur))
		if profondeur == 0:
			
			return (plateau.evalPlateau(self.couleurval), None,0)
			
		else :
			if maximisateur:
				valeur = float('-inf')
				couleur_val = self.couleurval
			else :
				valeur = float('inf')
				couleur_val = -self.couleurval
				
			liste_coups = plateau.liste_coups_valides(couleur_val)
			
			for coup in liste_coups:
				new_plateau = plateau.copie()
				new_plateau.jouer(coup, couleur_val)
				
				val_temp = self.min_max(new_plateau, not maximisateur, profondeur-1)

				if maximisateur :
					if val_temp[0] > valeur :
						valeur = val_temp[0]
						arg = coup
				
				else:
					if val_temp[0] < valeur :
						valeur = val_temp[0]
						arg = coup

			return (valeur, arg, len(liste_coups))
		
			


class AlphaBeta(IA):

	def demande_coup(self):

		time_debut = time.time()
		simu = self.alpha_beta(self.jeu.plateau, True,float('-inf'), float('inf'), 3)
		time_fin = time.time()
		delta = time_fin - time_debut

		
		nb_jouer = 0
		nb_jouer += simu [2]
		print("Pour ce coup,le joueur "+ self.couleur+ " a utiliser jouer "+ str(nb_jouer) +" fois et a mis " + str(delta) + " secondes")
		 
		coup_choisie = simu [1]
		return coup_choisie


	def alpha_beta(self, plateau, maximisateur, alpha, beta, profondeur) :
		if profondeur == 0 :

			return (plateau.evalPlateau(self.couleurval), None,0)

		else :
			if maximisateur :
				valeur = float('-inf')
				couleur_val = self.couleurval
			else :
				valeur = float('inf')
				couleur_val = -self.couleurval

			liste_coups = plateau.liste_coups_valides(couleur_val)

			for coup in liste_coups :
				new_plateau = plateau.copie()
				new_plateau.jouer(coup, couleur_val)

				val_temp = self.alpha_beta(new_plateau, not maximisateur, alpha, beta, profondeur-1)

				if maximisateur :
					if val_temp[0] > valeur :
						valeur = val_temp[0]
						arg = coup
						alpha = max(alpha, valeur)
				else:
					if val_temp[0] < valeur :
						valeur = val_temp[0]
						arg = coup
						beta = min(beta, valeur)
				
				if alpha >= beta :
						break	
			return(valeur, arg, len(liste_coups))	
