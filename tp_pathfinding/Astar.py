import graphe

g = graphe.lire_france()

def Astar( self, g, depart, arrive ) :
    
    #ajout les noueuds a E

    #E = {a | a est un noeud de g}
    dict_villes = {0 : depart}
    d = {}
    prec = {}
    f = {}

    def h(self, v) :
        return g.distance(depart, v)

    #graphe.lire_routes(g)

    #init dictionnaire
    for ville in range(1,len(g.edges)) :
        if g.edges[ville] != [] :
            dict_villes[ville] = g.edges[ville]

    #init valeur
    for ville in dict_villes :
        d[ville] = f[ville] = math.inf 
        prec[ville]= None
    d[depart] = 0
    f[depart] = h(depart)

    while dict_villes != {} :
        d_courte = math.inf
        
        for ville in dict_villes :
            if(d_courte > f[ville]) :
                d_courte = f[ville]
                voisin_proche = ville

        if voisin_proche == arrive :
            return ( d_courte, prec )

        del dict_villes[voisin_proche]

        for voisin_de_voisin_proche in g.edges[voisin_proche] :
            alt = d[voisin_proche] + voisin_de_voisin_proche[1]

            if alt < d[voisin_de_voisin_proche] :
                d[voisin_de_voisin_proche] = alt
                f[voisin_de_voisin_proche] = alt + h(voisin_de_voisin_proche)
                prec[voisin_de_voisin_proche] = voisin_proche 
                if voisin_de_voisin_proche not in dict_villes :
                    dict_villes = dict_villes + {voisin_de_voisin_proche}